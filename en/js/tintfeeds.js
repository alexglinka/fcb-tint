﻿$(document).ready(function () {
    var start_date = (new Date()).getTime();
    var end_date = (new Date()).getTime();
    var url = "http://api.tintup.com/v1/analytics?user_id=123777226&start_date=" + start_date + "&end_date=" + end_date + "&filter=day";
    $.ajax({
        type: 'GET',
        //url: 'https://api.tintup.com/v1/feed/nestlebaby?api_token=acc197bd07c04320458e1295d61798dd673825d7&count=62500',
        url: url,
        contentType: 'application/json; charset=utf-8',
        dataType: 'jsonp',
        timeout: 10000,
        success: fnFeedSuccess,
        error: function (msg) {
            // handle error        
        }
    });
});

function fnFeedSuccess(result) {
    var feedCount = pad(result.post_approved, 5);
    $('#feed-count').html(feedCount);
}

function pad(number, size) {
    var s = number + "";
    while (s.length < size) s = "0" + s;
    return s;
}
