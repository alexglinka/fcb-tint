$(document).ready(function(){

	var production = "http://media.nestle.ca/bi2/mobile/en";

	//========================
	// NAVIGATION CLOSE MODAL
	//========================

	$(".inner_nav li a").click(function(e){

		$('#navModal').foundation('reveal', 'close');
	});

	//==========================
	// OVERRIDE ACCORDION ANCHOR
	//==========================

	//The accordian headings that contain anchor links do not work.
	//The want to show/hide child elements by default, after all, it is an accordion.
	//This method allows the headlines or parent (dd elements) to be used
	//as links. 

	$(".accordion dd:not(:first-child)").on('click', function(e){

		e.preventDefault();

		var current = $(this).data("url");
		//window.location = $(this).data("url");

		if(current === "video.html")
		{
			window.open(current, "_self");
		}
		else
		{
			window.open(current,'_blank');
		}
		
	});

	

	var iPad = navigator.userAgent.indexOf("iPad")!=-1;

	//========================
	// SHOW/HIDE COPY FUNCTION
	//========================

	var copy_toggle = $(".copy-toggle");

	$(".copy-toggle").on('click', function(e){

		e.preventDefault();

		var self = $(this);
		var paragraph_id = $(this).data("id");
		var section = "#section" + paragraph_id;
		var element = $(this).closest(section).find("p");

		if(element.is(":hidden"))
		{
			self.text("Less Details");
			element.css("display", "inline-block", "important");
		}
		else
		{
			self.text("Full Details");
			element.css("display", "none");
		}
		
	});

	//======================
	// FACDE OUT SWIPE ICON
	//======================

	var swipe_icon = $("#swipe_icon");
	var distance = 80;

	if(iPad)
		distance = 300;


	$(window).on('scroll', function(){

		var scrollTop = $(window).scrollTop(),
		elementOffset = swipe_icon.offset().top,
		difference = (elementOffset - scrollTop);

		if(difference <= distance)
		{
			//swipe_icon.animate({opacity: 0});
			swipe_icon.css("display", "none");
		}

	});

	//=======
	// AUIDO
	//=======
	var audio_control = document.getElementById("audio");
	var play = $("#play");
	var status = $("#play").data("audio");

	play.click(function(e){

		e.preventDefault();

		if(status === "off")
		{
			audio_control.play();
			status = "on";
			$(this).find("img").attr("src", "http://media.nestle.ca/bi2/mobile/en/generic/infant_first_listen_story_iPad_pause.png");
		}
		else
		{
			audio_control.pause();
			status = "off";
			$(this).find("img").attr("src", "http://media.nestle.ca/bi2/mobile/en/generic/infant_first_listen_story_iPad.png");
		}
		
	});	

	//================
	// SCROLL TRACKING
	//================

	$('#section2').waypoint(function(direction) {
        
	_gaq.push(['_trackEvent', 'GS-Big-Idea', 'Scroll', 'S2-Professor']);

    });

    $('#section3').waypoint(function(direction) {
    _gaq.push(['_trackEvent', 'GS-Big-Idea', 'Scroll', 'S3-Hills']);
    });

    $('#section-slider').waypoint(function(direction) {
    _gaq.push(['_trackEvent', 'GS-Big-Idea', 'Scroll', 'Product-Carousel']);
    });

    $('#section4').waypoint(function(direction) {
    _gaq.push(['_trackEvent', 'GS-Big-Idea', 'Scroll', 'S4-Theatre']);
    });

    $('#section5').waypoint(function(direction) {
     _gaq.push(['_trackEvent', 'GS-Big-Idea', 'Scroll', 'S5-Clouds']);
    });

    $('#section6').waypoint(function(direction) {
    _gaq.push(['_trackEvent', 'GS-Big-Idea', 'Scroll', 'S6-Bathtub']);
    });

    $('#section7').waypoint(function(direction) {
    _gaq.push(['_trackEvent', 'GS-Big-Idea', 'Scroll', 'S7-GoodStart']);
    });

    $('#section-video').waypoint(function(direction) {
    _gaq.push(['_trackEvent', 'GS-Big-Idea', 'Scroll', 'NBP Registration']);
    });
	
  
});