//****choose your transition type, ex: blindX, blindY, blindZ, cover, curtainX, curtainY, fade, fadeZoom, growX, growY, scrollUp, scrollDown, scrollLeft, scrollRight, scrollHorz, scrollVert, shuffle, slideX, slideY, toss,turnUp, turnDown, turnLeft, turnRight, uncover, wipe, zoom****

$(function() {

	$.fn.cycle.transitions.scrollHorzFade = function($cont, $slides, opts) {
	//$cont.css('overflow','hidden').width();
	opts.before.push(function(curr, next, opts, fwd) {
		opts.cssBefore.left = fwd ? (next.cycleW-1) : (1-next.cycleW);
		opts.animOut.left = fwd ? -curr.cycleW : curr.cycleW;
	});
	opts.cssBefore= { opacity: 0, display: 'block' };
	opts.animIn   = { opacity: 1, left: 0 };
	opts.animOut  = { opacity: 0 };
};



    $('.slide-wrap')
	.after('<div class="slide-arrow">') 
	.cycle({ 
		fx:     'scrollHorzFade', 
		speed:    800,
		pause:     1,  
		timeout:  1000,
		next:   '#next', //For Next Image
		prev:   '#prev',
		easing: 'easeOutQuad',
		cleartype:false //For Previous Image
		//pager:  '.slide-bullet' // For Paging Or Button
		// return sel string for existing anchor // div
		/*pagerAnchorBuilder: function(idx, slide) {
            return '#slider_nav li:eq(' + (idx) + ') a';
        }*/
	});
});