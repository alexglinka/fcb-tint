
window.onload = function() {
	scriptPlace = document.getElementById('scrolling');
	/// Check for Safari ///
	isSafari = false;
	safari = navigator.userAgent.toLowerCase();
	if (safari.indexOf('safari') != -1) {
		if (safari.indexOf('chrome') == -1) {
			isSafari = true;
		}
	}

	/// Check for Firefox ///
	isFirefox = typeof InstallTrigger !== 'undefined';

	isIE9 = false;
	isIE10 = false;
	isIE11 = false;
	isOpera = false;
	if (navigator.appVersion.indexOf("MSIE 9.") != -1) {
		isIE9 = true;
	} else if (/*@cc_on!@*/false && document.documentMode === 10) {
		isIE10 = true;
	}
	if (navigator.userAgent.indexOf("OPR") != -1) {
		isOpera = true;
	}
	var ua = navigator.userAgent.toString().toLowerCase();
	var match = /(trident)(?:.*rv:([\w.]+))?/.exec(ua) || /(msie) ([\w.]+)/.exec(ua) || ['', null, -1];
	if (match[2] == '11.0') {
		isIE11 = true;
	}
	if (!isIE9 & !isIE10 & !isIE11 & !isOpera) {
		var newscript = document.createElement('script');
		newscript.type = 'text/javascript';
		newscript.async = true;
		newscript.src = 'scripts/scrollingCh.js';
		(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(newscript);
	}

	if (isIE9 || isIE10 || isOpera || isIE11) {

		/////////////////////////////////////////////
		/// Setters ///
		/// Page Length. Default 7100 ///
		var pageLength = 9100;
		/// Page Scrolling Repetition Rate in ms. Default 10 ///
		var scrollingRepetitionRate = 10;
		/// Timeline Animation Repetition Rate in ms. Except IE11! Default 10 ///
		var timelineRepetitionRate = 10;
		/// Timeline Animation Repetition Rate in ms. For IE11! Default 25 ///
		var timelineRepetitionRateIE11 = 25;
		/// Audio Track Length in seconds ///
		var audioTrackLength = 84;
		/// Background Audio Track Volume from 0.0 to 1.0. Default 0.8 ///
		var bgAudioTrackVolume = 0.1;
		/// Narrative Audio Track Volume from 0.0 to 1.0. Default 1.0 ///
		var narrativeAudioTrackVolume = 0.7;
		/// Timeline Width in px ///
		var timelineWidth = 100;
		/// Timeline Animation Step in px. Default 1.18 ///
		var timelineAnimationStep = 1.18;
		//////////////////////////////////////////////

		/// Detect Firefox ///
		isFirefox = typeof InstallTrigger !== 'undefined';
		if (isFirefox) {
			storyF = document.getElementById("story");
		}

		isOpera = false;
		if (navigator.userAgent.indexOf("OPR") > -1) {
			isOpera = true;
		}

		///	Variables ///
		var playButton = document.getElementById("play");
		var pauseButton = document.getElementById("pause");
		var purpleButton = document.getElementsByClassName("purple-btn")[0];
		var muteButton = document.getElementById("mute");
		var timeline = document.getElementById("timeline");
		var timelineBorder = document.getElementById("timelineBorder");
		story = document.getElementById("story");
		var mainAudio = document.getElementById("main_audio");
		var isPlaying = false;
		var isPlayButtonInMainAreaActive = true;

		/// CSS Settings For Timeline ///
	
		timeline.style.width = "0px";
		timeline.style.maxWidth = timelineWidth - 1 + "px";
		timeline.style.height = "3px";
		if (isOpera) {
		//	timeline.style.marginLeft = "375px";
		} else {
		//	timeline.style.marginLeft = "421px";
		}
		//timeline.style.marginTop = "4px";
		timeline.onmouseover = function() {
		//	timeline.style.cursor = "pointer";
		};
		timelineBorder.style.width = timelineWidth + 1 + "px";
		timelineBorder.style.height = "5px";
		if (isOpera) {
		//	timelineBorder.style.marginLeft = "374px";
		} else {
		//	timelineBorder.style.marginLeft = "420px";
		}
		//timelineBorder.style.marginTop = "-10px";
		timelineBorder.style.backgroundColor = "#5a107d";
		timelineBorder.onmouseover = function() {
		//	timelineBorder.style.cursor = "pointer";
		};
		if (isOpera & window.pageYOffset < 356) {
		//	timelineBorder.style.marginTop = "-11px";
		//	timeline.style.marginBottom = "1px";
		}
		
		if (isFirefox) {
			story = document.getElementById("story");
		//	timelineBorder.style.marginTop = "31px";
		}
		if (isSafari) {
		//	timeline.style.marginLeft = "605px";
		//	timelineBorder.style.marginLeft = "604px";
		}
		
mainAudio.volume = bgAudioTrackVolume;
		story.volume = narrativeAudioTrackVolume;
		var neverScrolled = true;

		/// Page Scrolling ///
		isMovingDown = false;
		function moveDown(arg) {
			if ( typeof arg === 'undefined') {
				arg = true;
			}
			if (arg) {
				isMovingDown = true;
				if (isIE11) {
					smooth = setTimeout(moveDown, timelineRepetitionRateIE11);
				} else {
					smooth = setTimeout(moveDown, timelineRepetitionRate);
				}
				if (window.pageYOffset > pageLength) {
					story.pause();
					isPlaying = false;
					enable_scroll();
					animateTimeLine(false);
					clearTimeout(smooth);
					playButton.style.display = "block";
					pauseButton.style.display = "none";
				}
				if (isIE11) {
					window.scroll(0, window.pageYOffset + 2);
				} else {
					setTimeout(function() {
						window.scroll(0, window.pageYOffset + 1);
					});
				}
			}
			if (arg == false) {
				isMovingDown = false;
				clearTimeout(smooth);
			}
		}

		/// Timeline Animation ///
		function animateTimeLine(arg) {
			if ( typeof arg === 'undefined') {
				arg = true;
			}
			if (arg) {
				timer = setTimeout(animateTimeLine, 10);
				if (isOpera) {
					timeline.style.width = parseFloat(timeline.style.width) + timelineAnimationStep / audioTrackLength + "px";
				} else {
					timeline.style.width = window.document.documentElement.scrollTop * timelineWidth / pageLength + "px";
				}
			}
			if (!arg) {
				clearTimeout(timer);
			}
		}

		/// Enable Scroll ///
		function enable_scroll() {
			if (window.removeEventListener) {
				window.removeEventListener('DOMMouseScroll', wheel, false);
				isPlaying = false;
			}
			window.onmousewheel = document.onmousewheel = document.onkeydown = null;
		}

		/// Play Button On The Page ///
		purpleButton.onclick = function mainMove() {
			if (isPlayButtonInMainAreaActive) {
				story.play();
				isPlaying = true;
				animateTimeLine();
				playButton.style.display = "none";
				pauseButton.style.display = "block";
				moveDown();
				story.currentTime = audioTrackLength * parseFloat(timeline.style.width) / timelineWidth;
				isPlayButtonInMainAreaActive = false;
			}
			return false;
		}
		/// Play Button In The Header ///
		playButton.onclick = function() {
			isPlayButtonInMainAreaActive = false;
			story.play();
			isPlaying = true;
			animateTimeLine();
		playButton.style.display = "none";
			pauseButton.style.display = "block";
			moveDown();
			story.currentTime = parseFloat(timeline.style.width) * audioTrackLength / timelineWidth;
			return false;
		};

		/// Pause Button In The Header ///
		pauseButton.onclick = function() {
			isPlayButtonInMainAreaActive = true;
			story.pause();
			moveDown(false);
			enable_scroll();
			isPlaying = false;
			animateTimeLine(false);
		playButton.style.display = "block";
		pauseButton.style.display = "none";
			return false;
		};

		var isMuted = false;
		/// Mute Button In The Header ///
		muteButton.onclick = function() {
			if (!isMuted) {
				mainAudio.volume = 0;
				isMuted = true;
			} else {
				mainAudio.volume = bgAudioTrackVolume;
				isMuted = false;
			}
			return false;
		};

		/// Prevent Default Functions ///
		function preventDefault(e) {
			e = e || window.event;
			if (e.preventDefault) {
				e.preventDefault();
			} else {
				e.returnValue = false;
			}
		}

		function wheel(e) {
			preventDefault(e);
		}

		function disable_scroll() {
			if (window.addEventListener) {
				window.addEventListener('DOMMouseScroll', wheel, false);
			}
			window.onmousewheel = document.onmousewheel = wheel;
			//document.onkeydown = keydown;
		}

		/// End Of Prevent Default Functions ///

		/// Pause if scrollbar moved ///
		prevOffsetInterval = setInterval(function() {
			prevOffset = window.pageYOffset;
		}, 50);

		checkOffsetInterval = setInterval(function() {
			currentOffset = window.pageYOffset;
			if (Math.abs(prevOffset - currentOffset) > 10) {
				isPlayButtonInMainAreaActive = true;
				story.pause();
				moveDown(false);
				enable_scroll();
				isPlaying = false;
				animateTimeLine(false);
			playButton.style.display = "block";
			pauseButton.style.display = "none";
			}
		}, 100);

		operaWasScrolled = false;

		window.onscroll = function scrolling() {
			if (isOpera & window.pageYOffset > 356) {
				timelineBorder.style.marginTop = "-10px";
				timeline.style.marginBottom = "0px";
				operaWasScrolled = true;
			}
			if (operaWasScrolled & window.pageYOffset < 356) {
				timelineBorder.style.marginTop = "29px";
				timeline.style.marginBottom = "-39px";
				operaWasScrolled = false;
			}
			if (!isPlaying) {
				if (!isTimelineClicked) {
					if (isOpera) {
						timeline.style.width = window.pageYOffset * timelineWidth / pageLength + "px";
						story.currentTime = audioTrackLength * parseFloat(timeline.style.width) / timelineWidth;
					} else {
						timeline.style.width = window.document.documentElement.scrollTop * timelineWidth / pageLength + "px";
						story.currentTime = story.duration * parseFloat(timeline.style.width) / timelineWidth;
					}
				}
			}
			if (isPlaying) {
				disable_scroll();
				if (isOpera & window.pageYOffset < 356) {
					timelineBorder.style.marginTop = "-11px";
					timeline.style.marginBottom = "1px";
					operaWasScrolled = true;
				}
			}
			if (story.currentTime > audioTrackLength - 1) {
				story.stop();
			}
		};
		/// IMPORTANT. If you will change timeline position variables in this function should be changed ///
		/// Getting x,y coordinates to control timeline clicks ///
		var isIE = document.all ? true : false;
		var isTimelineClicked = false;
		document.onclick = getMousePosition;
		function getMousePosition(e) {
			isTimelineClicked = true;
			setTimeout(function() {
				isTimelineClicked = false;
			}, 50);
			var _x = 0;
			var _y = 0;
			if (!isIE) {
				_x = e.pageX;
				_y = e.pageY - window.pageYOffset;
			}
			if (isIE) {
				_x = event.clientX + document.body.scrollLeft;
				_y = event.clientY + document.body.scrollTop;
			}
			var posX = _x;
			var posY = _y;
			/////////////////////////////////////////////////////////////////////////////
			/// To get coordinates for position of the timeline uncomment 4 lines of console.logs. ///
			/// Click the left edge of the timeline. ///
			/// Insert a new value of X in the timelineLeftEdge variable. Default 719 ///
			// console.log('posX');
			// console.log(posX);
			// console.log('posY');
			// console.log(posY);
			/////////////////////////////////////////////////////////////////////////////
			if (!isIE11) {
				var timelineLeftEdge = 720;
			} 
			if (isIE11) {
				var timelineLeftEdge = 762;
			}
			var timelineTopY = 71;
			var timelineBottomY = 79;
			if (isOpera) {
				if (document.documentElement.offsetWidth < 982) {
					targetX = timelineLeftEdge - Math.abs(1665 - 997) * 0.5;
				}
				if (document.documentElement.offsetWidth > 981 & document.documentElement.offsetWidth <= 1665) {
					targetX = timelineLeftEdge - Math.abs(1665 - document.documentElement.offsetWidth) * 0.5;
				}
				if (document.documentElement.offsetWidth > 1665) {
					targetX = 845 - Math.abs(1920 - document.documentElement.offsetWidth) * 0.5;
				}
			} else {
				if (document.documentElement.offsetWidth < 982) {
					targetX = timelineLeftEdge - Math.abs(1665 - 997) * 0.5;
				}
				if (document.documentElement.offsetWidth > 981 & document.documentElement.offsetWidth <= 1665) {
					targetX = timelineLeftEdge - Math.abs(1665 - document.documentElement.offsetWidth) * 0.5;
				}
				if (document.documentElement.offsetWidth > 1665) {
					targetX = 845 - Math.abs(1920 - document.documentElement.offsetWidth) * 0.5;
				}
			}
			if (posX > targetX && posX < targetX + timelineWidth & posY > timelineTopY && posY < timelineBottomY & isTimelineClicked) {
				var seek = posX - targetX;
				story.currentTime = story.duration * seek / timelineWidth;
				timeline.style.width = "0px";
				timeline.style.width = seek + "px";
				window.scrollTo(0, pageLength * seek / timelineWidth);
			}
		};
	}
};
