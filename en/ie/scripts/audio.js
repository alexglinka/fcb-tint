  var autoplaymovie = 0;
  var muteflag = 0;

  function getsoundplaying() {
      newvar = soundManager._writeDebug(this.id);
      //console.log("isplaying =" + newvar);
  }

  function mute() {
      if (muteflag == 0) {
          $(".sound-btn").hide();
          $(".mute-btn").show();
          muteflag = 1;
          stopAllSounds();
          soundManager.mute();
          autoplaymovie = 0;
          $("#autoplaymov").html("Play");

      } else {
          $(".sound-btn").show();
          $(".mute-btn").hide();
          muteflag = 0;
          $("#mute").html("Sound off");
          soundManager.unmute();
          soundManager.setVolume('sound0', 20);
      }
  }

  function stopAllSounds() {
      for (var i = 1; i < 9; i++) {
          soundManager.stop('sound' + i);
      }
  }

  function playsound(id) {
      autoplaymovie = 0;
      $("#autoplaymov").html("Play");
      stopAllSounds();
      soundManager.play('sound' + id);
  }

  function autoPlay() {

      if (autoplaymovie == 0) {
          if (muteflag == 1) {
              mute();
          }
          $(".play-btn").hide();
          $(".pause-btn").show();
          stopAllSounds();
          
          soundManager.setVolume('sound0', 4);
          autoplaymovie = 1;
          $("#autoplaymov").html("Pause");
          $('#sec1').click();
          soundManager.play('sound1', {
              onfinish: function() {
                  $('#sec2').click();

                  soundManager.play('sound2', {
                      onfinish: function() {
                          $('#sec3').click();
                          soundManager.play('sound3', {
                              onfinish: function() {
                                  $('#sec4').click();
                                  soundManager.play('sound4', {
                                      onfinish: function() {
                                          $('#sec5').click();
                                          soundManager.play('sound5', {
                                              onfinish: function() {
                                                  $('#sec6').click();
                                                  soundManager.play('sound6', {
                                                      onfinish: function() {
                                                          $('#sec7').click();
                                                          soundManager.play('sound7', {
                                                              onfinish: function() {
                                                                  $('#sec8').click();
                                                                  soundManager.play('sound8', {
                                                                      onfinish: function() {
                                                                          $('#join-nav').click();
                                                                          //alert('The sound '+this.id+' finished playing.');
                                                                          $("#autoplaymov").html("Play");
                                                                          autoplaymovie = 0;
                                                                          soundManager.setVolume('sound0', 20);
                                                                      }
                                                                  });
                                                              }
                                                          });
                                                      }
                                                  });
                                              }
                                          });
                                      }
                                  });
                              }
                          });
                      }
                  });
              }
          });
      } else {
          stopAllSounds();
          autoplaymovie = 0;
          $("#autoplaymov").html("Play");
          soundManager.setVolume('sound0', 20);
          $(".play-btn").show();
          $(".pause-btn").hide();
      }


      _gaq.push(['_trackEvent', 'GS-Big-Idea', 'Play-Button']);

  }

  soundManager.setup({
      url: '../swf/',
      flashVersion: 9,
      consoleOnly: true,
      preferFlash: false, // prefer 100% HTML5 mode, where both supported
      onready: function() {
          // console.log('SM2 ready!');
          var mySound = soundManager.createSound({
              url: 'http://media.nestle.ca/bi2/desk/bigidea/audio/Nestle_Full_Instrumental.mp3'

          });



          for (var i = 1; i < 9; i++) {

              // var enURL = 'http://clients/rivet/nestlegsparallax/bigidea/audio/Nestle_GoodStartBigIdea_ENGLISH_segment_0' + i + '.mp3';
              // var frURL = 'http://clients/rivet/nestlegsparallax/bigidea/audio/Nestle_GoodStartBigIdea_FR_segment_0' + i + '.mp3';
              var enURL = 'http://media.nestle.ca/bi2/desk/bigidea/audio/Nestle_GoodStartBigIdea_ENGLISH_segment_0' + i + '.mp3';
              var frURL = 'http://media.nestle.ca/bi2/desk/bigidea/audio/Nestle_GoodStartBigIdea_FR_segment_0' + i + '.mp3';
              var narrativeUrl = '';
              if (language === "fr") {
                  narrativeUrl = frURL;
              } else {
                  narrativeUrl = enURL;
              }

              soundManager.createSound({

                  url: narrativeUrl,
                  autoLoad: true,
                  autoPlay: false,
                  volume: 50
              });

          }

          mySound.play({
              loops: 1000
          });
      },
      ontimeout: function() {
          //  console.log('SM2 init failed!');
      },
      defaultOptions: {
          // set global default volume for all sound objects
          volume: 20,

      }
  });