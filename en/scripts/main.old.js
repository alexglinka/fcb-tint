/* SCENE 1 */
var stopani = 0;
function fade_mom(){
	$(".mom-leans").delay(2000).fadeOut(500);
	$(".mom-standing").delay(2000).fadeIn(500, function(){
		if(stopani==0){
		leans_mom();	
}	else {
				$(".first-butterflyTxt").delay(500).animate({ top : "185px", left:"-70px" }, 1000);
			}
	});
}
function leans_mom(){
	$(".mom-standing").delay(2000).fadeOut(500);
	$(".mom-leans").delay(2000).fadeIn(500, function(){
		stopani=1;
		fade_mom();	
	});
}
function fade_mom5(){
	var mom = $(".mom-mod5-container");
	var data_mom = $(mom).data("mom");
	if(data_mom == 'open'){
		$(mom).find(".mom-mod5-open").fadeOut(500);
		$(mom).find(".mom-mod5").fadeIn(500);
		$(mom).data("mom", "close");
	}else{
		$(mom).find(".mom-mod5").fadeOut(500);
		$(mom).find(".mom-mod5-open").fadeIn(500);
		$(mom).data("mom", "open");		
	}
}
function diagonal_left(){
	var newcat = $(".green-fish");
	$(newcat).animate({ top : "-160px", left: "400px" }, 1000, function(){
		$(this).animate({ top : "-70px", left : "200px" }, 1400, function(){
			$(this).animate({ left : "550px" }, 400, function(){ diagonal_left(); });
		});
	});
}

function diagonal_right(){
	var newcat = $(".blue-shark");
	$(newcat).animate({ top : "-160px", left: "400px" }, 1000);
		$(newcat).animate({ top : "-70px", left : "650px" }, 1400, function(){
			$(this).animate({ left : "200px" }, 400, function(){ diagonal_right(); });
		});
}

function show_up(arg, px, time, delays){
	$(arg).delay(delays).animate({ top : "-=" + px }, time, function(){
		$(this).delay(delays).animate({ top : "+=" + px }, time, function(){
			show_up(arg, px, time, delays);
		});	
	});
}
function check_bird(arg){

	var check = $(arg).hasClass("active");
	if(check){
		$(arg).removeClass("active").addClass("deactive");
	}else{
		$(arg).removeClass("deactive").addClass("active");		
	}
}


/* BUTTERFLY ANIMATION */
var red=1;
var pink=1;
var green = 1;
var green8 = 1;
var red8 = 1;

function greenb() {
	var rest = $(".green-butterfly").hasClass("hide");
	if(green >= 2){
		if(rest) {
			$(".green-butterfly").removeClass("hide").addClass("show");	
		}else{
			$(".green-butterfly").removeClass("show").addClass("hide");
		}
	}	
	green +=1;
	if(green >= 10) {
		green = 1;	
	}
}

function greenb8() {
	var rest = $(".green-butterfly-mod8").hasClass("hide");
	if(green8 >= 2){
		if(rest) {
			$(".green-butterfly-mod8").removeClass("hide").addClass("show");	
		}else{
			$(".green-butterfly-mod8").removeClass("show").addClass("hide");
		}
	}	
	green8 +=1;
	if(green8 >= 10) {
		green8 = 1;	
	}
}

function redb8() {
	var rest = $(".red-butterfly-mod8").hasClass("hide");
	
	if(red8 <= 10){
		if(rest) {
			$(".red-butterfly-mod8").removeClass("hide").addClass("show");	
		}else{
			$(".red-butterfly-mod8").removeClass("show").addClass("hide");
		}
	}
	
	red8 +=1;
	if(red8 >= 12) {
		red8 = 1;	
	}
}

function redb() {
	var rest = $(".red-butterfly").hasClass("hide");
	
	if(red <= 10){
		if(rest) {
			$(".red-butterfly").removeClass("hide").addClass("show");	
		}else{
			$(".red-butterfly").removeClass("show").addClass("hide");
		}
	}
	
	red +=1;
	if(red >= 12) {
		red = 1;	
	}
}

function pinkb() {
	var rest = $(".pink-butterfly").hasClass("hide");
	
	if(pink > 7){
		if(rest) {
			$(".pink-butterfly").removeClass("hide").addClass("show");	
		}else{
			$(".pink-butterfly").removeClass("show").addClass("hide");
		}
	}
	
	pink +=1;
	if(pink >= 14) {
		pink = 1;	
	}
}

function makeNewPosition(arg, id){
	var green = [[300,100],[120,350]];
	var pink = [[400, -40], [400, 320]];
	var red = [[50, 100], [250, 0]];
	if(arg == 'green'){
    	return [green[id][0],green[id][1]];    
	}else if(arg == 'pink') {
    	return [pink[id][0],pink[id][1]];    		
	}else{
    	return [red[id][0],red[id][1]];    				
	}
}

var animategreenindex = 0;

function animategreen(id){
	if(id == 1){
		animategreenindex = 0;
	}else{
		animategreenindex = 1;
	}
    var newq = makeNewPosition('green', id);
	if(id == 1){
    $(".green-butterfly").delay(800).removeClass("reverse").addClass("normal").animate({ top: newq[0], left: newq[1] }, 3000, function(){
		animategreen(animategreenindex);	
	}); 
	}else{
    $(".green-butterfly").delay(800).removeClass("normal").addClass("reverse").animate({ top: newq[0], left: newq[1] }, 3000, function(){
		animategreen(animategreenindex);	
	}); 	
	}
};

var animatepinkindex = 0;

function animatepink(id){
	if(id == 1){
		animatepinkindex = 0;
	}else{
		animatepinkindex = 1;
	}
    var newq = makeNewPosition('pink', id);
	if(id == 1){
    $(".pink-butterfly").removeClass("reverse").addClass("normal").animate({ top: newq[0], left: newq[1] }, 5000, function(){
		animatepink(animatepinkindex);	
	}); 
	}else{
    $(".pink-butterfly").removeClass("normal").addClass("reverse").animate({ top: newq[0], left: newq[1] }, 5000, function(){
		animatepink(animatepinkindex);	
	}); 	
	}
};

var animateredindex = 0;

function animatered(id){
	if(id == 1){
		animateredindex = 0;
	}else{
		animateredindex = 1;
	} 
    var newq = makeNewPosition('red', id);
	if(id == 1){
    $(".red-butterfly").removeClass("reverse").addClass("normal").animate({ top: newq[0], left: newq[1] }, 6000, function(){
		animatered(animateredindex);	
	}); 
	}else{
    $(".red-butterfly").removeClass("normal").addClass("reverse").animate({ top: newq[0], left: newq[1] }, 6000, function(){
		animatered(animateredindex);	
	}); 	
	}
};

var stopani8 = 0;
function canfade(){
	//$(".module8-titleTxt").delay(2000).animate({ opacity : 1 }, 200);
	if (stopani8==0){
		$(".module8-product-large").delay(1000).fadeOut(1000);
		$(".module8-product-real").delay(1000).fadeIn(1000, function() {
			$(".module8-titleTxt").animate({ opacity : 1 }, 200);});
		stopani8=1;
	};
	$(".red-butterfly-mod8").animate({ opacity : 1 }, 200);
	$(".green-butterfly-mod8").animate({ opacity : 1 }, 200);
	 TweenMax.to($(".red-butterfly-mod8"), 7, {css:{left:"1350px",top:"200px", rotation:"-100px"}, ease:"Liner.easeNone"});
	 TweenMax.to($(".green-butterfly-mod8"), 7, {css:{left:"1550px",top:"0px", rotation:"60px"},delay:1, ease:"Liner.easeNone",onComplete:fadeButterflies});
}

 function fadeButterflies(){
 TweenMax.to($(".red-butterfly-mod8"), .01, {css:{left:"-500px",top:"350px", rotation:"0px",opacity:"0"}, ease:"Liner.easeNone"});
	 TweenMax.to($(".green-butterfly-mod8"), .01, {css:{left:"-500px",top:"370px", rotation:"0px",opacity:"0"}, ease:"Liner.easeNone"});
	}

function canfadeout(){
	//$(".module8-titleTxt").animate({ opacity : 0 }, 20);	
	//$(".module8-product-real").fadeOut(100, function() {
	//	$(".module8-titleTxt").animate({ opacity : 0 }, 20);});
	//$(".module8-product-large").fadeIn(100);
	//$(".red-butterfly-mod8").css({top: '350px', left: '-500px', rotation:"0px"});
	//$(".green-butterfly-mod8").css({top: '370px', left: '-500px', rotation:"0px"});
}
function pricefade(){
	$(".module8-book, .module8-milk").stop().animate({ opacity : 1 }, 200, function(){
		$(".module8-product-pot").stop().animate({ opacity : 1 }, 300);
		$(".module8-product-packet").stop().animate({  opacity : 1 }, 400);		
		$(".module8-price-tag").stop().animate({ opacity : 1 }, 500);	
	});
}

function pricefadeout(){
	$(".module8-book, .module8-milk").stop().animate({ opacity : 0 }, 200);
	$(".module8-price-tag").stop().animate({ opacity : 0 }, 200);	
	$(".module8-product-pot").stop().animate({  opacity : 0 }, 200);
	$(".module8-product-packet").stop().animate({  opacity : 0 }, 200);
}
function formulaleft(){
	$(".first-formulaTxt").stop().animate({ left : "40px" }, 500);
}

function formularight(){
	$(".first-formulaTxt").stop().animate({ left : "1000px" }, 500);
}

function mod5_shadow(){
	$(".baby-mod5-shadow").fadeIn(200);
}

function mod5_shadowout(){
	$(".baby-mod5-shadow").fadeOut(200);
}

function mod6_shadow(){
	$(".baby-cloud-overlay, .cloud-baby-shadow").fadeIn(200);
}

function mod6_shadowout(){
	$(".baby-cloud-overlay, .cloud-baby-shadow").fadeOut(200);
}

function cloud_sheep(){
	$(".cloud-sheep").animate({ right : "60px" }, 500);
}

function cloud_sheep_out(){
	$(".cloud-sheep").animate({ right : "-350px" }, 500);
}

/* PARALLAX MOVEMENT */
var slide_bottom=function(arg, top, speed, fast, start, end){
	$(window).scroll(function(){
	var x=$(window).scrollTop();
	var dir=top+((x-start)/speed)*fast;
	if((x >= start) && (x <= end)){
		$(arg).css({ "top":dir + 'px' });			
	}else{
		$(arg).css({ "top":top + 'px' });						
	}
	});

}

var frombottom=function(arg, start, end, speed, boost){
var arg_bottom = parseInt($(arg).css("top").replace('px', ''));
var top_count = 0;
var dirr, selisih;
$(window).scroll(function(){
	top_count = $(this).scrollTop();
	if(top_count >= start){
		dirr = arg_bottom - ((top_count - start)/speed) * boost;
		if(dirr >= end){
			$(arg).css({ top : dirr + 'px' });
		}else{
			$(arg).css({ top : end + 'px' });
		}
	}
});

}

var frombottomwcall=function(arg, start, end, speed, boost, before, after){
var arg_bottom = parseInt($(arg).css("top").replace('px', ''));
var top_count = 0;
var dirr, selisih;
var t = 1;
$(window).scroll(function(){
	top_count = $(this).scrollTop();
	if(top_count >= start){
		dirr = arg_bottom - ((top_count - start)/speed) * boost;
		if(dirr >= end){
			if(t != 1){
				before();
				t = 1;
			}
			$(arg).css({ top : dirr + 'px' });
		}else{
			$(arg).css({ top : end + 'px' });
			var right_arg = parseInt($(arg).css("right").replace('px', ''));
			if(t != 2){
				after();
				t = 2;
			}
		}
	}
});
}

var frombottom2=function(arg, start, end, speed, boost, kurang){
var arg_top = parseInt($(arg).css("top").replace('px', ''));
var height = $(window).height();
var position = $(arg).offset().top;
if(kurang != '' || kurang != null){
	var top = (position - height) - kurang;
}else{
	var top = position - height;
}
var top_count = 0;
var dirr, selisih;
$(window).scroll(function(){
	top_count = $(this).scrollTop();
	if(top_count >= top){
		selisih = top_count - top;
		dirr = arg_top - (selisih/speed) * boost;
		if(dirr >= end){
			$(arg).css({ top : dirr + 'px' });
		}else{
			$(arg).css({ top : end + 'px' });
		}
	}
});
}

function frombottom3(arg, start, end, speed, boost, kurang, before, after){
var arg_top = parseInt($(arg).css("top").replace('px', ''));
var height = $(window).height();
var position = $(arg).offset().top;
var i = 1;
if(kurang != '' || kurang != null){
	var top = (position - height) - kurang;
}else{
	var top = position - height;
}
var top_count = 0;
var dirr, selisih;
$(window).scroll(function(){
	top_count = $(this).scrollTop();
	if(top_count >= top){
		selisih = top_count - top;
		dirr = arg_top - (selisih/speed) * boost;
		if(dirr >= end){
			if(i != 1) {
				before();				
				i = 1;	
			}
			$(arg).css({ top : dirr + 'px' });
		}else{
			$(arg).css({ top : end + 'px' });
			after();
			i = 2;
		}
	}
});
}

var tobottom=function(arg, start, end, speed, boost, kurang){
var arg_top = parseInt($(arg).css("top").replace('px', ''));
var height = $(window).height();
var position = $(arg).offset().top;
if(kurang != '' || kurang != null){
	var top = (position - height) - kurang;
}else{
	var top = position - height;
}
var top_count = 0;
var dirr, selisih;
$(window).scroll(function(){
	top_count = $(this).scrollTop();
	if(top_count >= top){
		selisih = top_count - top;
		dirr = arg_top + (selisih/speed) * boost;
		if(dirr <= end){
			$(arg).css({ top : dirr + 'px' });
		}else{
			$(arg).css({ top : end + 'px' });
		}
	}else if(top_count < top){
			$(arg).css({ top : arg_top + 'px' });		
	}
});
}

var continous=function(arg, start, pause, px, time, end, speed, boost){
var height_window = $(window).height();
var height_page = $(document).height();
var arg_offset = $(arg).offset();
var arg_height = $(arg).height();
var top_parent = parseInt($(arg).css("top").replace('px', ''));
var top_count = 0;
var dirr, selisih, new_count;
$(window).scroll(function(){
	top_count = $(this).scrollTop();
	new_count = top_count - start;
	if(top_count >= start && top_count < pause){
		dirr = top_parent + ((new_count/speed)*boost);
		$(arg).css({ top : dirr + "px" });
	}else if(top_count >= pause && top_count < (pause + time)){
		$(arg).css({ top : px + "px" });
	}else if(top_count >= (pause + time)){
		dirr = top_parent + (((new_count/speed)*boost) - ((time/speed)*boost));
		if(dirr < end){
			$(arg).css({ top : dirr + "px" });
		}else{
			$(arg).css({ top : end + "px" });			
		}
	}	
});
}

var continous2=function(arg, start, pause, px, time, end, speed, boost, before, after){
var height_window = $(window).height();
var height_page = $(document).height();
var arg_offset = $(arg).offset();
var arg_height = $(arg).height();
var top_parent = parseInt($(arg).css("top").replace('px', ''));
var top_count = 0;
var dirr, selisih, new_count;
$(window).scroll(function(){
	top_count = $(this).scrollTop();
	new_count = top_count - start;
	if(top_count >= start && top_count < pause){
		dirr = top_parent + ((new_count/speed)*boost);
		$(arg).css({ top : dirr + "px" });
		before();
	}else if(top_count >= pause && top_count < (pause + time)){
		$(arg).css({ top : px + "px" });
		after();
	}else if(top_count >= (pause + time)){
		dirr = top_parent + (((new_count/speed)*boost) - ((time/speed)*boost));
		if(dirr < end){
			$(arg).css({ top : dirr + "px" });
		}else{
			$(arg).css({ top : end + "px" });			
		}
	}	
});
}

var continous_back=function(arg, start, pause, px, time, end, speed, boost, speed2, boost2, endpx, beforeb, afterb){
var top_parent = parseInt($(arg).css("top").replace('px', ''));
var top_count = 0;
var dirr, selisih, new_count;
var tb = 1;
$(window).scroll(function(){
	top_count = $(this).scrollTop();
	selisih = top_count - start;
	if((top_count > start) && (top_count < pause)){
		dirr = top_parent + ((selisih/speed)*boost);
		if((tb != 1) && (afterb != '')){
			afterb();
			tb = 1;
		}
		$(arg).css({ top : dirr + "px" });
	}else if((top_count >= pause) && (top_count < (pause + time))){
		$(arg).css({ top : px + "px" });
		if((tb != 2) && (beforeb != '')){
			beforeb();
			tb = 2;
		}
	}else if(top_count >= (pause + time)){
		new_count = (( top_count-(pause + time) )/speed2) * boost2;
		if((tb != 1) && (afterb != '')){
			afterb();
			tb = 1;
		}
		if(new_count < endpx){
			dirr = px - new_count;
		}else{
			dirr = px - endpx;
		}
			$(arg).css({ top : dirr + "px" });		

	}else{
		$(arg).css({ top : top_parent + "px" });			
		if((tb != 1) && (afterb != '')){
			afterb();
			tb = 1;
		}
	}
});
}

var continousfrombottom=function(arg, start, pause, px, time, end, speed, boost, speed2, boost2, endpx, before, after){
var top_parent = parseInt($(arg).css("top").replace('px', ''));
var top_count = 0;
var dirr, selisih, new_count;
var tc = 1;
$(window).scroll(function(){
	top_count = $(this).scrollTop();
	selisih = top_count - start;
	if((top_count > start) && (top_count < pause)){
		dirr = top_parent - ((selisih/speed)*boost);
		if((tc != 1) && (after != '')){
			after();
			tc = 1;
		}
		$(arg).css({ top : dirr + "px" });
	}else if((top_count >= pause) && (top_count < (pause + time))){
		if((tc != 2) && (before != '')){
			before();
			tc = 2;
		}
		$(arg).css({ top : px + "px" });
	}else if(top_count >= (pause + time)){
		new_count = (( top_count-(pause + time) )/speed2) * boost2;

		if(new_count < endpx){
			dirr = px - new_count;
		}else{
			dirr = px - endpx;
		}
		
		$(arg).css({ top : dirr + "px" });
	}else{
		if((tc != 1) && (after != '')){
			after();
			tc = 1;
		}
		$(arg).css({ top : top_parent + "px" });			
	}

});
}

var bottom=function(arg, start, end, speed, boost){
var height_window = $(window).height();
var height_page = $(document).height();
var arg_offset = $(arg).offset();
var arg_height = $(arg).height();
var arg_bottom = parseInt($(arg).css("top").replace('px', ''));
var top_parent = arg_offset.top - height_window;
var top_count = 0;
var dirr, selisih;
$(window).scroll(function(){
	top_count = $(this).scrollTop();
	selisih = top_count - top_parent;
	if(top_count >= top_parent){
		dirr = arg_bottom + ((selisih/speed)*boost);
		if(dirr < end){
			$(arg).css({ top : dirr + "px" });
		}else{
			$(arg).css({ top : end + "px" });			
		}
	}
});
}

var right=function(arg, start, end, speed, boost, kurang){
var height_window = $(window).height();
var arg_bottom = parseInt($(arg).css("right").replace('px', ''));
var top_contact = $(arg).offset();
var top_scroll = 0;
if(kurang != null || kurang != ''){
	var top_item = (top_contact.top - height_window) - kurang;
}else{
	var top_item = top_contact.top - height_window;	
}
$(window).scroll(function(){
	top_scroll = $(this).scrollTop();
	if(top_scroll > top_item){
		var selisih = top_scroll - top_item;
		var dirr=arg_bottom + ((selisih/speed)*boost);
		if(dirr < end){
			$(arg).css({ right : dirr + "px" });
		}else{
			$(arg).css({ right : end + "px" });		
		}
	}
});
}

var right2=function(arg, start, end, speed, boost){
var height_window = $(window).height();
var arg_bottom = parseInt($(arg).css("right").replace('px', ''));
var top_contact = $(arg).offset();
var top_scroll = 0;
$(window).scroll(function(){
	top_scroll = $(this).scrollTop();
	if(top_scroll > start){
		var selisih = top_scroll - start;
		var dirr=arg_bottom + ((selisih/speed)*boost);
		if(dirr < end){
			$(arg).css({ right : dirr + "px" });
		}else{
			$(arg).css({ right : end + "px" });		
		}
	}else{
			$(arg).css({ right : arg_bottom + "px" });				
	}
});
}

var left=function(arg, start, end, speed, boost, kurang, tambah){
var height_window = $(window).height();
var top_contact = $(arg).offset();
var arg_bottom = parseInt($(arg).css("left").replace('px', ''));
var top_scroll = 0;
if((kurang != '' || kurang != null) && (tambah == '' || tambah == null)){
	var top_item = (top_contact.top - height_window) - kurang;
}else if((tambah != '' || tambah != null) && (kurang == '' || kurang == null)){
	var top_item = (top_contact.top - height_window) + tambah;
}else{
	var top_item = top_contact.top - height_window;
}
$(window).scroll(function(){
	top_scroll = $(this).scrollTop();
	if(top_scroll > top_item){
		var selisih = top_scroll - top_item;
		var dirr=arg_bottom + ((selisih/speed)*boost);
		if((dirr < end) && (selisih > 0)){
			$(arg).css({ left : dirr + "px" });
		}else if(selisih <= 0){
			$(arg).css({ left : arg_bottom + "px" });
		}else{
			$(arg).css({ left : end + "px" });			
		}
	}
});
}

var fadein=function(arg, speed, kurang){
var height_window = $(window).height();
var top_contact = $(arg).offset();
var top_scroll = 0;
if(kurang != null || kurang != ''){
	var top_item = (top_contact.top - height_window) - kurang;
}else{
	var top_item = top_contact.top - height_window;
}
$(window).scroll(function(){
	top_scroll = $(this).scrollTop();
	if(top_scroll > top_item){
		var selisih = top_scroll - top_item;
		var dirr=0 + ((selisih/speed));
		if((dirr <= 1) && (selisih > 0)){
			$(arg).css({ opacity : dirr });
		}else if(selisih <= 0){
			$(arg).css({ opacity: 0 });
		}else{
			$(arg).css({ opacity : 1 });
		}
	}
});
}

var rotate=function(arg, add, start, end, speed, tambah){
var height_window = $(window).height();
var top_contact = $(arg).offset();
var top_scroll = 0;
if(tambah != null || tambah != ''){
	var top_item = (top_contact.top - height_window) + tambah;
}else{
	var top_item = top_contact.top - height_window;
}
$(window).scroll(function(){
	top_scroll = $(this).scrollTop();
	if(top_scroll >= top_item){
		var selisih = top_scroll - top_item;
		if(add == 'plus'){
			var dirr=start + ((selisih/speed));		
	
		if(dirr < end){
			$(arg).css({ 
						"transform" : "rotate(" + dirr +"deg)",
						"-ms-transform" : "rotate(" + dirr + "deg)",
						"-webkit-transform" : "rotate("+ dirr +"deg)"
					});
		}else{
			$(arg).css({ 
						"transform" : "rotate(" + end +"deg)",
						"-ms-transform" : "rotate(" + end + "deg)",
						"-webkit-transform" : "rotate("+ end +"deg)"			
					});			
		}
		
		}else{
			var dirr=start - ((selisih/speed));			

		if(dirr >= end){
			$(arg).css({ 
						"transform" : "rotate(" + dirr +"deg)",
						"-ms-transform" : "rotate(" + dirr + "deg)",
						"-webkit-transform" : "rotate("+ dirr +"deg)"			
			 });
		}else{
			$(arg).css({
						"transform" : "rotate(" + end +"deg)",
						"-ms-transform" : "rotate(" + end + "deg)",
						"-webkit-transform" : "rotate("+ end +"deg)"							
				});			
		}

		}
		
	}
});
}


$(function(){

/* scene 1 */

		var mom_standing = $(".mom-standing");
		continousfrombottom(mom_standing, 0, 0, 30, 100, 0, 3, 4, 3, 6, 200, '', '');
		
		var babymod1_container = $(".babymod1-container");
		continousfrombottom(babymod1_container, 0, 0, 0, 100, 0, 3, 4, 4, 3, 200, '', '');

		var module1_caption = $(".module1-caption");
		continousfrombottom(module1_caption, 0, 0, 0, 100, 0, 3, 4, 6, 2, 200, '', '');

		var purple_bar = $(".purple-bar");
		continousfrombottom(purple_bar, 0, 0, 770, 100, 0, 3, 4, 4, 3, 140, '', '');

/* scene 2 */

		var baby = $(".baby-masked");
		continous_back(baby, 0, 490, 205,  300, 205, 3, 5, 3, 6, 350, '', '');

		var mom_mod2_container = $(".mom-mod2-container");
		continousfrombottom(mom_mod2_container, 300, 490, 0, 300, 0, 3, 4, 3, 6, 350, '', '');

		var man_2 = $(".man2-container");
		continousfrombottom(man_2, 200, 580, 0, 210, 0, 3, 6, 4, 4, 200, formulaleft, formularight);

		var mountain = $(".mountain");
		bottom(mountain, 300, 20, 10, 2);

		var curtain_container = $(".curtain-container");
		continous_back(curtain_container, 300, 400, -30,  390, 205, 5, 2, 6, 2, 200, '', '');

/* scene 3 */

		var cloud_left = $(".cloud-left");
		left(cloud_left, 0, 750, 10, 3, '', '');

		var cloud_right = $(".cloud-right");
		right(cloud_right, 0, 750, 6, 3, '');

		var baby_mod3 = $(".baby-mod3");
		continous_back(baby_mod3, 550, 1340, 350,  200, 450, 3, 4, 3, 9, 450, '', '');

		var blanket = $(".blanket");
		continousfrombottom(blanket, 1540, 1540, 390, 0, 0, 3, 4, 4, 4, 200, '', '');

		var fronthill_container = $(".fronthill-container");
		continousfrombottom(fronthill_container, 1540, 1540, -50, 0, 0, 3, 4, 4, 4, 200, '', '');

		var sun = $(".sun");
		bottom(sun, 0, -50, 5, 2, '');
		
		var bird = $(".bird-mod3");
		right(bird, 0, 550, 6, 2, '');

		var cow = $(".cow");
		left(cow, 0, 150, 5, 3, 0, -800);

		var bunny = $(".bunny");
		left(bunny, 0, 780, 3, 4, 110, '');

		var first_achooTxt = $(".first-achooTxt");
		fadein(first_achooTxt, 150, 80);
		left(first_achooTxt, 0, 1240, 3, 4, 100, '');
		frombottom2(first_achooTxt, 7000, 430, 3, 4, 100);

		var backhill = $(".backhill-container");
		frombottom(backhill, 1100, 30, 2, 6);

		var module3_footer = $(".module3-footer");
		continousfrombottom(module3_footer, 1540, 1540, 2200, 0, 0, 3, 4, 4, 4, 300, '', '');

/* scene 5 */

		var baby_mod5 = $(".baby-mod5");
		continous_back(baby_mod5, 2210, 2700, 300,  250, 300, 3, 4, 3, 8, 400, mod5_shadow, mod5_shadowout);

		var left_curtain_mod5 = $(".left-curtain-mod5");
		tobottom(left_curtain_mod5, 7000, 0, 4, 3, -200);

		var right_curtain_mod5 = $(".right-curtain-mod5");
		tobottom(right_curtain_mod5, 7000, 0, 5, 3, -200);
			
		var light1 = $(".light1");
		rotate(light1, 'plus', 300, 360, 2, 350);

		var light2 = $(".light2");
		rotate(light2, 'plus', 300, 360, 2, 350);

		var module5_floor = $(".module5-floor");
		continousfrombottom(module5_floor, 2950, 2950, 0, 0, 0, 3, 4, 4, 5, 280, '', '');

		var module5_content_overlay = $(".module5-content-overlay");
		fadein(module5_content_overlay, 100, -400);

		var module5_container = $(".module5-continous");
		continousfrombottom(module5_container, 2950, 2950, 0, 0, 0, 3, 4, 4, 5, 280, '', '');

		var first_bellyTxt = $(".first-bellyTxt");
		fadein(first_bellyTxt, 300, '');
		right(first_bellyTxt, 0, 365, 4, 2, '');
		bottom(first_bellyTxt, 0, 310, 4, 2, '');
		
/* scene 6 */

		var cloud_baby_container = $(".cloud-baby-container");
		continous_back(cloud_baby_container, 2900, 3410, 370,  200, 500, 2, 3, 3, 8, 350, mod6_shadow, mod6_shadowout);

		var moon = $(".moon");
		bottom(moon, 0, 0, 5, 4, -200);

		var frontCloud = $(".frontCloud");
		continousfrombottom(frontCloud, 2750, 3350, 300, 200, 0, 8, 4, 4, 4, 80, '', '');

		var backCloud = $(".backCloud");
		continousfrombottom(backCloud, 3320, 3380, 350, 200, 0, 2, 7, 4, 2, 150, cloud_sheep, cloud_sheep_out);

		var star_container = $(".star-container");
		continous_back(star_container, 2500, 3560, 180,  150, 500, 6, 3, 6, 2, 300, '', '');

		var cloud_cat = $(".cloud-sheep");
		continousfrombottom(cloud_cat, 3380, 3380, 350, 200, 0, 6, 4, 4, 2, 150, '', '');

		var container_text_mod6 = $(".container-text-mod6");
		frombottom2(container_text_mod6, 7000, 100, 3, 6, 350);

/* scene 7 */

		var bath_baby = $(".bath-baby");
		continous_back(bath_baby, 3700, 4060, 230, 300, 500, 3, 6, 4, 4, 0, '', '');

		var bathup_container = $(".bathup-container");
		continousfrombottom(bathup_container, 3905, 4000, 410, 260, 0, 2, 7, 4, 4, 0, '', '');

		var banner_tag = $(".banner-tag");
		right2(banner_tag, 3890, 320, 4, 5);
		continousfrombottom(banner_tag, 4360, 4360, 0, 0, 0, 3, 4, 4, 3, 100, '', '');

		var bird_container = $(".bird-container");
		right2(bird_container, 3890, 2000, 4, 5);

		var bath_mom_container = $(".bath-mom-container");
		frombottom2(bath_mom_container, 7000, 60, 3, 7, 220);

		var megical_containers = $(".megical-containers");
		fadein(megical_containers, 100, 200);
				
		var first_magical = $(".first-magicalTxt");
		tobottom(first_magical, 0, 0, 6, 3, 200);

		var seashore = $(".seashore");
		show_up(seashore, 35, 300, 1800);

		var crab = $(".crab");
		show_up(crab, 20, 200, 1000);

		var module7_wall = $(".module7-wall");
		continousfrombottom(module7_wall, 4360, 4360, 0, 0, 0, 3, 4, 6, 2, 100, '', '');

		var module7_continous = $(".module7-continous");
		continousfrombottom(module7_continous, 4360, 4360, -20, 40, 0, 3, 4, 4, 4, 150, '', '');

		var module7_footer = $(".module7-footer");
		continousfrombottom(module7_footer, 4000, 4000, 5050, 400, 0, 2, 7, 4, 4, 240, '', '');

		diagonal_right();
		diagonal_left();
		
/* scene 8 */

		var the_baby_mod8 = $(".the-baby-mod8");
		continous(the_baby_mod8, 4700, 4935, 120, 2000, 130, 3, 5);
		
		var baby_crib_mod8 = $(".baby-crib-mod8");
		frombottom(baby_crib_mod8, 6200, 20, 8, 2);

		var module8_bottom_img = $(".module8-bottom-img");
		frombottom3(module8_bottom_img, 7000, 0, 3, 4, 230 , pricefadeout, pricefade);

		var module8_can_container = $(".module8-can-container");
		frombottom3(module8_can_container, 7000, 0, 2, 2, 300 ,canfadeout, canfade);

});






$(window).load(function(){

var module2_footer = $(".module2-footer");
var check_class = $(module2_footer).hasClass("french");
if(check_class){
		continousfrombottom(module2_footer, 580, 580, 1390, 210, 0, 3, 4, 4, 4, 250, '', '');
}else{
		continousfrombottom(module2_footer, 580, 580, 1390, 210, 0, 3, 4, 4, 4, 200, '', '');	
}

var module5_footer = $(".module5-footer");
	continousfrombottom(module5_footer, 2950, 2950, 3540, 0, 0, 3, 4, 4, 4, 300, '', '');

var module6_footer = $(".module6-footer");
var check_class6 = $(module6_footer).hasClass("french");
if(check_class6){
	continousfrombottom(module6_footer, 3500, 3500, 4350, 200, 0, 2, 7, 4, 4, 310, '', '');
}else{
	continousfrombottom(module6_footer, 3500, 3500, 4350, 200, 0, 2, 7, 4, 4, 250, '', '');
}

//Bezier curves:
var bezierCurves = {
	greenButterfly 		: {type:"cubic", values:[{x:100, y:250}, {x:150, y:100}, {x:300, y:500}, {x:500, y:400}], autoRotate:["x","y","rotation", 0, true]},
	redButterfly 		: {type:"cubic", values:[{x:100, y:250}, {x:150, y:100}, {x:300, y:500}, {x:500, y:400}], autoRotate:["x","y","rotation", 0, true]}
};

//Mother animations:

var module_1_Timeline 	= new TimelineLite(),
	$motherStanding 	= $(".mom-standing"),
	$motherLeaning  	= $(".mom-leans"),
	$greenButterfly 	= $(".green-butterfly"),
	$redButterfly 		= $(".red-butterfly");

	module_1_Timeline.appendMultiple([
		TweenLite.to($motherStanding, 	1, {css:{opacity:"0"}}),
		TweenLite.to($motherLeaning, 	1, {css:{opacity:"1"},onComplete:startButterflies})
	],2);

	module_1_Timeline.appendMultiple([
		TweenMax.to($greenButterfly, 5, {bezier:bezierCurves.greenButterfly, ease:Power1.easeInOut}),
		TweenMax.to($redButterfly,	 5, {bezier:bezierCurves.redButterfly, ease:Power1.easeInOut}),
	]);

	module_1_Timeline.to($(".module1-caption"), 1, {css:{opacity:"1"}, ease:"Liner.easeNone"});

	module_1_Timeline.appendMultiple([
		TweenLite.to($motherStanding, 		1, {css:{opacity:"1"}}),
			TweenLite.to($motherLeaning, 	1, {css:{opacity:"0"}})
	],2);

	module_1_Timeline.to($(".first-butterflyTxt"), 1, {css:{left:"-70px",top:"185px"}, ease:"Power3.easeOut", delay:10});


function startButterflies(){

	window.greenstart 	= setInterval(greenb, 150);
	window.redstart 	= setInterval(redb, 150);

}

function frame2(){
	//To replace the butterfly animations which require a callback at each point, the replacement code will use setTimeout();

	//TweenMax.to($(".green-butterfly"), .1, {css:{rotation:"180px"},delay:0,onComplete:frame4});
 	//TweenMax.to($(".red-butterfly"), 1.2, {css:{rotation:"0px"},delay:1, onComplete:frame4a});
	
	setTimeout(frame4,0.1);
	setTimeout(frame4a,1.2);



}

function frame3(){




}

function frame4(){
	//TweenMax.to($(".module1-caption"), 1, {css:{opacity:"1"}, ease:"Liner.easeNone"})
	//TweenMax.to($(".green-butterfly"), 2, {css:{left:"-50px",top:"450px"}, delay:0, ease:"Liner.easeNone",onComplete:frame5});
	setTimeout(frame5,2);
}

function frame4a(){

		//TweenMax.to($(".red-butterfly"), 2.2, {css:{left:"-40px",top:"420px"}, delay:1.5, ease:"Liner.easeNone",onComplete:frame5a});
}

function frame5(){
  	//TweenMax.set($(".green-butterfly"), {css:{zIndex:15}});
  	//TweenMax.to($(".green-butterfly"), .5, {css:{rotation:"50px"}, ease:"Liner.easeNone"});
	//TweenMax.to($(".green-butterfly"), 2, {css:{left:"400px",top:"450px"}, delay:.5, ease:"Liner.easeNone",onComplete:frame6});
	setTimeout(frame6,2);
}

function frame5a(){
  	//TweenMax.set($(".red-butterfly"), {css:{zIndex:15}});
  	//TweenMax.to($(".red-butterfly"), .5, {css:{rotation:"-120px"}, delay: .2, ease:"Liner.easeNone"});
	//TweenMax.to($(".red-butterfly"), 2.2, {css:{left:"550px",top:"450px"}, delay:.4, ease:"Liner.easeNone",onComplete:frame6a});
	setTimeout(frame6a,2.6);
}


function frame6(){
	//TweenMax.to($(".green-butterfly"), .5, {css:{rotation:"-50px"}, ease:"Liner.easeNone"});
 	//TweenMax.to($(".green-butterfly"), 2, {css:{left:"390px",top:"50px"}, delay:.1, ease:"Liner.easeNone",onComplete:frame7});
 	setTimeout(frame7,2.1);
}

function frame6a(){
	//TweenMax.to($(".red-butterfly"), .5, {css:{rotation:"-220px"}, delay:.6, ease:"Liner.easeNone"});
 	//TweenMax.to($(".red-butterfly"), 2, {css:{left:"380px",top:"25px"}, delay:.7, ease:"Liner.easeNone",onComplete:frame7a});
}

function frame7(){
 //TweenMax.to($(".green-butterfly"), .5, {css:{rotation:"-100px"}, ease:"Liner.easeNone"});
 //TweenMax.to($(".green-butterfly"), 2, {css:{left:"-50px",top:"-200px",rotation:"-100px"}, delay:.5, ease:"Liner.easeNone",onComplete:frame8});
	  
//$(".first-butterflyTxt").delay(500).animate({ top : "185px", left:"-70px" }, 1000);

	  


	
	}

	function frame7a(){
 //TweenMax.to($(".red-butterfly"), .5, {css:{rotation:"60px"}, delay:.5, ease:"Liner.easeNone"});
 //TweenMax.to($(".red-butterfly"), 2, {css:{left:"-70px",top:"-210px",rotation:"80px"}, delay:.6, ease:"Liner.easeNone"});


	}


function frame8(){
 //TweenMax.to($(".green-butterfly"), .5, {css:{rotation:"-90px"}, ease:"Liner.easeNone"});
 //TweenMax.to($(".green-butterfly"), 1, {css:{left:"-150px",top:"-250px"}, delay:.2, ease:"Liner.easeNone",onComplete:frame9});

 //TweenMax.to($(".purple-ribbon"), 1, {css:{top:"auto"}, delay:.3, ease:"Power1.easeOut"});
//TweenMax.to($(".welcomeTxt"), 1, {css:{top:"auto"}, delay:.3, ease:"Power1.easeOut"});
//TweenMax.to($(".world-of-firsts"), 1, {css: {top:"auto"}, delay:.3, ease:"Power1.easeOut"});



}

function frame8a(){
 //TweenMax.to($(".red-butterfly"), .5, {css:{rotation:"150px"}, delay:.2, ease:"Liner.easeNone"});
 //TweenMax.to($(".red-butterfly"), 1, {css:{left:"-350px",top:"-200px"}, ease:"Liner.easeNone"});
   clearInterval(redstart);

}


function frame9(){
 clearInterval(greenstart);


 
}

//append a to() tween



	//TweenLite.to(logo, 1, {css:{left:"632px"}});


	/*
	$(".the-baby, .mom-standing").fadeIn(500, function(){
		fade_mom();

		$(".module1-caption").delay(1000).fadeIn(500);

	});

	$(".green-butterfly, .red-butterfly, .pink-butterfly").delay(1500).fadeIn(500, function(){$(".green-butterfly, .red-butterfly, .pink-butterfly").delay(3500).fadeOut(100);});
	
	
	animategreen(1);
	animatepink(1);
	animatered(1);
	
	var greenstart = setInterval(function(){
		greenb();
	}, 150);
	
	var redstart = setInterval(function(){
		redb();
	}, 200);
	
	var pinkstart = setInterval(function(){
		pinkb();
	}, 250);
*/
	var green8start = setInterval(function(){
		greenb8();
	}, 100);
	
	

	var red8start = setInterval(function(){
		redb8();
	}, 100);

	var fademom5 = setInterval(function(){
		fade_mom5();
	}, 1500);

	setInterval(function(){
		check_bird('.bird1');
		check_bird('.bird2');
		check_bird('.bird-mod3');
	}, 300);


});

/*
 *
 * The code below has been added to the original to make modifications. The orignial code has no comments and lacks a coherent structure and therefore it seems best to keep the code seperate.
 *
 */

