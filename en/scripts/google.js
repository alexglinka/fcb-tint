var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-32913938-1'], ['_trackPageview'], ['_gat._anonymizeIp'], ['_setDomainName', 'http://firsts.nestle-baby.ca'], ['_nesglobalhqtag._setAccount', 'UA-27534376-1'], ['_nesglobalhqtag._setCustomVar', 1, 'Zone', 'AMS', 3], ['_nesglobalhqtag._setCustomVar', 2, 'Country', 'Canada', 3], ['_nesglobalhqtag._setCustomVar', 3, 'Business', 'Maternal and Infant Nutrition', 3], ['_nesglobalhqtag._setCustomVar', 4, 'Brand', 'Nestle', 3], ['_nesglobalhqtag._setCustomVar', 5, 'Site', 'Nestle Baby Canada', 3], ['_nesglobalhqtag._setCustomVar', 6, 'Properties', 'Brand', 3], ['_nesglobalhqtag._setCustomVar', 7, 'Type', 'Site', 3], ['_nesglobalhqtag._setCustomVar', 8, 'U', location.pathname.substr(0, 63), 3], ['_nesglobalhqtag._setCustomVar', 9, 'Language', 'English', 3], ['_nesglobalhqtag._trackPageview', '/ams/canada/nestle baby canada']);

(function() {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
})();

$(window).scroll(function() {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
        //alert("bottom!");
        _gaq.push(['_trackEvent', 'GS-Big-Idea', 'Scrolled-To-Bottom']);
    }
});