/* PARALLAX MOVEMENT */
var slide_bottom = function (arg, top, speed, fast, start, end) {
    arg = $(arg);
    $(window).scroll(function () {
        var x = $(window).scrollTop();
        var dir = top + ((x - start) / speed) * fast;
        if ((x >= start) && (x <= end)) {
            arg.css({
                "top": dir + 'px'
            });
        } else {
            arg.css({
                "top": top + 'px'
            });
        }
    });

}

var frombottom = function (arg, start, end, speed, boost) {
    arg = $(arg);
    var arg_bottom = parseInt(arg.css("top").replace('px', ''));
    var top_count = 0;
    var dirr, selisih;
    $(window).scroll(function () {
        top_count = $(this).scrollTop();
        if (top_count >= start) {
            dirr = arg_bottom - ((top_count - start) / speed) * boost;
            if (dirr >= end) {
                arg.css({
                    top: dirr + 'px'
                });
            } else {
                arg.css({
                    top: end + 'px'
                });
            }
        }
    });

}

var frombottomwcall = function (arg, start, end, speed, boost, before, after) {
    arg = $(arg);
    var arg_bottom = parseInt(arg.css("top").replace('px', ''));
    var top_count = 0;
    var dirr, selisih;
    var t = 1;
    $(window).scroll(function () {
        top_count = $(this).scrollTop();
        if (top_count >= start) {
            dirr = arg_bottom - ((top_count - start) / speed) * boost;
            if (dirr >= end) {
                if (t != 1) {
                    before();
                    t = 1;
                }
                arg.css({
                    top: dirr + 'px'
                });
            } else {
                arg.css({
                    top: end + 'px'
                });
                var right_arg = parseInt(arg.css("right").replace('px', ''));
                if (t != 2) {
                    after();
                    t = 2;
                }
            }
        }
    });
}

var frombottom2 = function (arg, start, end, speed, boost, kurang) {
    arg = $(arg);
    var arg_top = parseInt(arg.css("top").replace('px', ''));
    var height = 630;
    var position = arg.offset().top;
    if (kurang != '' || kurang != null) {
        var top = (position - height) - kurang;
    } else {
        var top = position - height;
    }
    var top_count = 0;
    var dirr, selisih;
    $(window).scroll(function () {
        top_count = $(this).scrollTop();
        if (top_count >= top) {
            selisih = top_count - top;
            dirr = arg_top - (selisih / speed) * boost;
            if (dirr >= end) {
                arg.css({
                    top: dirr + 'px'
                });
            } else {
                arg.css({
                    top: end + 'px'
                });
            }
        }
    });
}

    function frombottom3(arg, start, end, speed, boost, kurang, before, after) {
        arg = $(arg);
        var arg_top = parseInt(arg.css("top").replace('px', ''));
        var height = $(window).height();
        var position = arg.offset().top;
        var i = 1;
        if (kurang != '' || kurang != null) {
            var top = (position - height) - kurang;
        } else {
            var top = position - height;
        }
        var top_count = 0;
        var dirr, selisih;
        $(window).scroll(function () {
            top_count = $(this).scrollTop();
            if (top_count >= top) {
                selisih = top_count - top;
                dirr = arg_top - (selisih / speed) * boost;
                if (dirr >= end) {
                    if (i != 1) {
                        before();
                        i = 1;
                    }
                    arg.css({
                        top: dirr + 'px'
                    });
                } else {
                    arg.css({
                        top: end + 'px'
                    });
                    after();
                    i = 2;
                }
            }
        });
    }

var tobottom = function (arg, start, end, speed, boost, kurang) {
    arg = $(arg);
    var arg_top = parseInt(arg.css("top").replace('px', ''));
    var height = $(window).height();
    var position = arg.offset().top;
    if (kurang != '' || kurang != null) {
        var top = (position - height) - kurang;
    } else {
        var top = position - height;
    }
    var top_count = 0;
    var dirr, selisih;
    $(window).scroll(function () {
        top_count = $(this).scrollTop();
        if (top_count >= top) {
            selisih = top_count - top;
            dirr = arg_top + (selisih / speed) * boost;
            if (dirr <= end) {
                arg.css({
                    top: dirr + 'px'
                });
            } else {
                arg.css({
                    top: end + 'px'
                });
            }
        } else if (top_count < top) {
            arg.css({
                top: arg_top + 'px'
            });
        }
    });
}

var continous = function (arg, start, pause, px, time, end, speed, boost) {
    arg = $(arg);
    var height_window = $(window).height();
    var height_page = $(document).height();
    var arg_offset = arg.offset();
    var arg_height = arg.height();
    var top_parent = parseInt(arg.css("top").replace('px', ''));
    var top_count = 0;
    var dirr, selisih, new_count;
    $(window).scroll(function () {
        top_count = $(this).scrollTop();
        new_count = top_count - start;
        if (top_count >= start && top_count < pause) {
            dirr = top_parent + ((new_count / speed) * boost);
            arg.css({
                top: dirr + "px"
            });
        } else if (top_count >= pause && top_count < (pause + time)) {
            arg.css({
                top: px + "px"
            });
        } else if (top_count >= (pause + time)) {
            dirr = top_parent + (((new_count / speed) * boost) - ((time / speed) * boost));
            if (dirr < end) {
                arg.css({
                    top: dirr + "px"
                });
            } else {
                arg.css({
                    top: end + "px"
                });
            }
        }
    });
}

var continous2 = function (arg, start, pause, px, time, end, speed, boost, before, after) {
    arg = $(arg);
    var height_window = $(window).height();
    var height_page = $(document).height();
    var arg_offset = arg.offset();
    var arg_height = arg.height();
    var top_parent = parseInt(arg.css("top").replace('px', ''));
    var top_count = 0;
    var dirr, selisih, new_count;
    $(window).scroll(function () {
        top_count = $(this).scrollTop();
        new_count = top_count - start;
        if (top_count >= start && top_count < pause) {
            dirr = top_parent + ((new_count / speed) * boost);
            arg.css({
                top: dirr + "px"
            });
            before();
        } else if (top_count >= pause && top_count < (pause + time)) {
            arg.css({
                top: px + "px"
            });
            after();
        } else if (top_count >= (pause + time)) {
            dirr = top_parent + (((new_count / speed) * boost) - ((time / speed) * boost));
            if (dirr < end) {
                arg.css({
                    top: dirr + "px"
                });
            } else {
                arg.css({
                    top: end + "px"
                });
            }
        }
    });
}

var continous_back = function (arg, start, pause, px, time, end, speed, boost, speed2, boost2, endpx, beforeb, afterb) {
    arg = $(arg);
    var top_parent = parseInt(arg.css("top").replace('px', ''));
    var top_count = 0;
    var dirr, selisih, new_count;
    var tb = 1;
    $(window).scroll(function () {
        top_count = $(this).scrollTop();
        selisih = top_count - start;
        if ((top_count > start) && (top_count < pause)) {
            dirr = top_parent + ((selisih / speed) * boost);
            if ((tb != 1) && (afterb != '')) {
                afterb();
                tb = 1;
            }
            arg.css({
                top: dirr + "px"
            });
        } else if ((top_count >= pause) && (top_count < (pause + time))) {
            arg.css({
                top: px + "px"
            });
            if ((tb != 2) && (beforeb != '')) {
                beforeb();
                tb = 2;
            }
        } else if (top_count >= (pause + time)) {
            new_count = ((top_count - (pause + time)) / speed2) * boost2;
            if ((tb != 1) && (afterb != '')) {
                afterb();
                tb = 1;
            }
            if (new_count < endpx) {
                dirr = px - new_count;
            } else {
                dirr = px - endpx;
            }
            arg.css({
                top: dirr + "px"
            });

        } else {
            arg.css({
                top: top_parent + "px"
            });
            if ((tb != 1) && (afterb != '')) {
                afterb();
                tb = 1;
            }
        }
    });
}

var continousfrombottom = function (arg, start, pause, px, time, end, speed, boost, speed2, boost2, endpx, before, after) {
    arg = $(arg);
    var top_parent = parseInt(arg.css("top").replace('px', ''));
    var top_count = 0;
    var dirr, selisih, new_count;
    var tc = 1;
    $(window).scroll(function () {
        top_count = $(this).scrollTop();
        selisih = top_count - start;
        if ((top_count > start) && (top_count < pause)) {
            dirr = top_parent - ((selisih / speed) * boost);
            if ((tc != 1) && (after != '')) {
                after();
                tc = 1;
            }
            arg.css({
                top: dirr + "px"
            });
        } else if ((top_count >= pause) && (top_count < (pause + time))) {
            if ((tc != 2) && (before != '')) {
                before();
                tc = 2;
            }
            arg.css({
                top: px + "px"
            });
        } else if (top_count >= (pause + time)) {
            new_count = ((top_count - (pause + time)) / speed2) * boost2;

            if (new_count < endpx) {
                dirr = px - new_count;
            } else {
                dirr = px - endpx;
            }

            arg.css({
                top: dirr + "px"
            });
        } else {
            if ((tc != 1) && (after != '')) {
                after();
                tc = 1;
            }
            arg.css({
                top: top_parent + "px"
            });
        }

    });
}

var bottom = function (arg, start, end, speed, boost) {
    arg = $(arg);
    var height_window = $(window).height();
    var height_page = $(document).height();
    var arg_offset = arg.offset();
    var arg_height = arg.height();
    var arg_bottom = parseInt(arg.css("top").replace('px', ''));
    var top_parent = arg_offset.top - height_window;
    var top_count = 0;
    var dirr, selisih;
    $(window).scroll(function () {
        top_count = $(this).scrollTop();
        selisih = top_count - top_parent;
        if (top_count >= top_parent) {
            dirr = arg_bottom + ((selisih / speed) * boost);
            if (dirr < end) {
                arg.css({
                    top: dirr + "px"
                });
            } else {
                arg.css({
                    top: end + "px"
                });
            }
        }
    });
}

var right = function (arg, start, end, speed, boost, kurang) {
    arg = $(arg);
    var height_window = 630;
    var arg_bottom = parseInt(arg.css("right").replace('px', ''));
    var top_contact = arg.offset();
    var top_scroll = 0;
    if (kurang != null || kurang != '') {
        var top_item = (top_contact.top - height_window) - kurang;
    } else {
        var top_item = top_contact.top - height_window;
    }
    $(window).scroll(function () {
        top_scroll = $(this).scrollTop();
        if (top_scroll > top_item) {
            var selisih = top_scroll - top_item;
            var dirr = arg_bottom + ((selisih / speed) * boost);
            if (dirr < end) {
                arg.css({
                    right: dirr + "px"
                });
            } else {
                arg.css({
                    right: end + "px"
                });
            }
        }
    });
}

var right2 = function (arg, start, end, speed, boost) {
    arg = $(arg);
    var height_window = 630;
    var arg_bottom = parseInt(arg.css("right").replace('px', ''));
    var top_contact = arg.offset();
    var top_scroll = 0;

    $(window).scroll(function () {
        //console.log("Top scroll: " + top_scroll);
        //console.log(arg.selector + ": " + arg.css("right"));

        top_scroll = $(this).scrollTop();
        if (top_scroll > start) {
            var selisih = top_scroll - start;
            var dirr = arg_bottom + ((selisih / speed) * boost);
            if (dirr < end) {
                arg.css({
                    right: dirr + "px"
                });
            } else {
                arg.css({
                    right: end + "px"
                });
            }
        } else {
            arg.css({
                right: arg_bottom + "px"
            });
        }
        
    });
}

var left = function (arg, start, end, speed, boost, kurang, tambah) {
    arg = $(arg);
    var height_window = $(window).height();
    var top_contact = arg.offset();
    var arg_bottom = parseInt(arg.css("left").replace('px', ''));
    var top_scroll = 0;
    if ((kurang != '' || kurang != null) && (tambah == '' || tambah == null)) {
        var top_item = (top_contact.top - height_window) - kurang;
    } else if ((tambah != '' || tambah != null) && (kurang == '' || kurang == null)) {
        var top_item = (top_contact.top - height_window) + tambah;
    } else {
        var top_item = top_contact.top - height_window;
    }
    $(window).scroll(function () {
        top_scroll = $(this).scrollTop();
        if (top_scroll > top_item) {
            var selisih = top_scroll - top_item;
            var dirr = arg_bottom + ((selisih / speed) * boost);
            if ((dirr < end) && (selisih > 0)) {
                arg.css({
                    left: dirr + "px"
                });
            } else if (selisih <= 0) {
                arg.css({
                    left: arg_bottom + "px"
                });
            } else {
                arg.css({
                    left: end + "px"
                });
            }
        }
    });
}

var fadein = function (arg, speed, kurang) {
    arg = $(arg);
    var height_window = $(window).height();
    var top_contact = arg.offset();
    var top_scroll = 0;
    if (kurang != null || kurang != '') {
        var top_item = (top_contact.top - height_window) - kurang;
    } else {
        var top_item = top_contact.top - height_window;
    }
    $(window).scroll(function () {
        top_scroll = $(this).scrollTop();
        if (top_scroll > top_item) {
            var selisih = top_scroll - top_item;
            var dirr = 0 + ((selisih / speed));
            if ((dirr <= 1) && (selisih > 0)) {
                arg.css({
                    opacity: dirr
                });
            } else if (selisih <= 0) {
                arg.css({
                    opacity: 0
                });
            } else {
                arg.css({
                    opacity: 1
                });
            }
        }
    });
}

var rotate = function (arg, add, start, end, speed, tambah) {
    arg = $(arg);
    var height_window = $(window).height();
    var top_contact = arg.offset();
    var top_scroll = 0;
    if (tambah != null || tambah != '') {
        var top_item = (top_contact.top - height_window) + tambah;
    } else {
        var top_item = top_contact.top - height_window;
    }
    $(window).scroll(function () {
        top_scroll = $(this).scrollTop();
        if (top_scroll >= top_item) {
            var selisih = top_scroll - top_item;
            if (add == 'plus') {
                var dirr = start + ((selisih / speed));

                if (dirr < end) {
                    arg.css({
                        "transform": "rotate(" + dirr + "deg)",
                        "-ms-transform": "rotate(" + dirr + "deg)",
                        "-webkit-transform": "rotate(" + dirr + "deg)"
                    });
                } else {
                    arg.css({
                        "transform": "rotate(" + end + "deg)",
                        "-ms-transform": "rotate(" + end + "deg)",
                        "-webkit-transform": "rotate(" + end + "deg)"
                    });
                }

            } else {
                var dirr = start - ((selisih / speed));

                if (dirr >= end) {
                    arg.css({
                        "transform": "rotate(" + dirr + "deg)",
                        "-ms-transform": "rotate(" + dirr + "deg)",
                        "-webkit-transform": "rotate(" + dirr + "deg)"
                    });
                } else {
                    arg.css({
                        "transform": "rotate(" + end + "deg)",
                        "-ms-transform": "rotate(" + end + "deg)",
                        "-webkit-transform": "rotate(" + end + "deg)"
                    });
                }

            }

        }
    });
}