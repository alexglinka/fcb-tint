$(function(){
    (function($, TweenMax, TweenLite, TimelineLite, _nestle_global) {

        /* SCROLL TRACKING */

        $('#sec-2').waypoint(function(direction) {
            if(direction === "down") _gaq.push(['_trackEvent', 'GS-Big-Idea', 'Scroll', 'S2-Professor']);
        });

        $('#sec-3').waypoint(function(direction) {
            if(direction === "down") _gaq.push(['_trackEvent', 'GS-Big-Idea', 'Scroll', 'S3-Hills']);
        });

        $('#sec-4').waypoint(function(direction) {
            if(direction === "down") _gaq.push(['_trackEvent', 'GS-Big-Idea', 'Scroll', 'Product-Carousel']);
        });

        $('#sec-5').waypoint(function(direction) {
            if(direction === "down") _gaq.push(['_trackEvent', 'GS-Big-Idea', 'Scroll', 'S4-Theatre']);
        });

        $('#sec-6').waypoint(function(direction) {
            if(direction === "down") _gaq.push(['_trackEvent', 'GS-Big-Idea', 'Scroll', 'S5-Clouds']);
        });

        $('#sec-7').waypoint(function(direction) {
            if(direction === "down") _gaq.push(['_trackEvent', 'GS-Big-Idea', 'Scroll', 'S6-Bathtub']);
        });

        $('#sec-8').waypoint(function(direction) {
            if(direction === "down") _gaq.push(['_trackEvent', 'GS-Big-Idea', 'Scroll', 'S7-GoodStart']);
        });

        $('#sec-9').waypoint(function(direction) {
            if(direction === "down") _gaq.push(['_trackEvent', 'GS-Big-Idea', 'Scroll', 'NBP Registration']);
        });

        /* FOR DEBUGGING DURING DEVELOPMENT: */

        //$(window).scroll(function () { console.log($(this).scrollTop()) });

        /*
    		For animations We'll be using TweenLite and TimelineLite: http://api.greensock.com/js/

    	 	We'll use two TimelineLite features here. First its regular use of transitions using TweenLite's .to and .fromTo methods.Second the .appendMultiple method which takes an array of TweenLite method returns. 

    	 	Without the appendMultiple, TimelineLite will perform each animation and once it's done it will perform the next. With appenMultiple, TimelineLite will perform all animations inside at the same time. Unlike using TweenLite methods directly on the TimelineLite object, using appendMultiple won't wait for animation completion, so if you have a sequential animation proceeding the multiple, you'll need to add a delay to it. The second parameter of appendMultiple is the delay to perform the animation.
    	*/

        //create new instance of the global method
        var _global = new _nestle_global();

        //CONTINUOUS ANIMATIONS
        /*
    		There are a number of animations throughout the page that keep animating on a loop. They're defined here
    	*/

        var handPuppetsAnimation = _global.anim_FadeElements($(".mom-mod5-container"));
        var bird1Animation = _global.anim_toggleClass($('.bird1'), 300);
        var bird2Animation = _global.anim_toggleClass($('.bird2'), 300);
        var bird3Animation = _global.anim_toggleClass($('.bird-mod3'), 300);

        TweenMax.to($(".green-fish"), 2.5, {
            bezier: _global.bezierCurves.scene7.greenFish,
            delay: 2,
            repeat: -1,
            repeatDelay: 3
        });
        TweenMax.to($(".blue-shark"), 2.5, {
            bezier: _global.bezierCurves.scene7.blueFish,
            delay: 2,
            repeat: -1,
            repeatDelay: 3
        });
        TweenMax.to($(".seashore"), 0.5, {
            top: "-100",
            repeat: -1,
            yoyo: true,
            repeatDelay: 1,
            delay: 0.4
        });
        TweenMax.to($(".crab"), 0.5, {
            top: "-55",
            repeat: -1,
            yoyo: true,
            repeatDelay: 1.2
        });

        //INNER ANIMATIONS
        /*
    	 	These animations happen within the scenes are defined in methods to be called as callbacks later in the animation definitions.
    	*/

        function firstFrameAnimations() {

            /*
    			This is by far the most complex single frame of animation. 
    		*/

            //define the variables used in this block of code
            var module_1_Timeline = new TimelineLite(),
                $motherStanding = $(".mom-standing"),
                $motherLeaning = $(".mom-leans"),
                $greenButterfly = $(".green-butterfly"),
                $redButterfly = $(".red-butterfly");

            //Mother leans down
            module_1_Timeline.appendMultiple([
                TweenLite.to($motherStanding, 1, {
                    css: {
                        opacity: "0"
                    }
                }),
                TweenLite.to($motherLeaning, 1, {
                    css: {
                        opacity: "1"
                    }
                })
            ], 1);

            //Mother leans up, butterflies start fluttering (within the flutter method is a method to kill the animation when it completes)
            module_1_Timeline.appendMultiple([
                TweenLite.to($motherStanding, 1, {
                    css: {
                        opacity: "1"
                    }
                }),
                TweenLite.to($motherLeaning, 1, {
                    css: {
                        opacity: "0"
                    },
                    onComplete: function() {
                        _global.flutter({
                            target: ".green-butterfly",
                            duration: 8,
                            delay: 0,
                            path: _global.bezierCurves.scene1.greenButterfly
                        });
                        _global.flutter({
                            target: ".red-butterfly",
                            duration: 7,
                            delay: 4,
                            path: _global.bezierCurves.scene1.redButterfly
                        });
                    }
                })
            ], 2);

            //Butterfly kisses text slides up from bottom
            module_1_Timeline.to($(".first-butterflyTxt"), 1, {
                css: {
                    left: "-56px",
                    top: "142px"
                },
                ease: "Power3.easeOut",
                delay: 5
            });

            //The main module text fades in
            module_1_Timeline.to($(".module1-caption"), 1, {
                css: {
                    opacity: "1"
                },
                ease: "Liner.easeNone"
            });

        }

        var can = {

            fade: function() {

                if (can.started) return;

                can.started = true;

                TweenMax.to($(".module8-product-large"), 1, {
                    opacity: 0,
                    delay: 1
                });
                TweenMax.to($(".module8-product-real"), 1, {
                    opacity: 1,
                    delay: 1
                });
                TweenMax.to($(".module8-titleTxt"), 1, {
                    opacity: 1,
                    delay: 2
                });

                _global.flutter({
                    target: ".red-butterfly-mod8",
                    duration: 12,
                    delay: 0,
                    path: _global.bezierCurves.scene8.redButterfly
                });

                _global.flutter({
                    target: ".green-butterfly-mod8",
                    duration: 13,
                    delay: 2,
                    path: _global.bezierCurves.scene8.greenButterfly
                });

            },

            started: false
        }

            function fadeButterflies() {
                TweenMax.to($(".red-butterfly-mod8"), .01, {
                    css: {
                        left: "-500px",
                        top: "350px",
                        rotation: "0px",
                        opacity: "0"
                    },
                    ease: "Liner.easeNone"
                });
                TweenMax.to($(".green-butterfly-mod8"), .01, {
                    css: {
                        left: "-500px",
                        top: "370px",
                        rotation: "0px",
                        opacity: "0"
                    },
                    ease: "Liner.easeNone"
                });
            }

        var priceFade = {
            $elements: $(".module8-book, .module8-milk, .module8-product-pot, .module8-product-packet, .module8-price-tag"),
            visible: false,
            timeline: new TimelineLite(),
            _in: function() {
                if (!priceFade.visible) {
                    priceFade.timeline.staggerTo(priceFade.$elements, 0.5, {
                        opacity: 1,
                        delay: 0.5
                    }, 0.2);
                    priceFade.visible = true;
                };
            },
            _out: function() {
                if (priceFade.visible) {
                    TweenLite.to(priceFade.$elements, 0.2, {
                        opacity: 0
                    });
                    priceFade.visible = false;
                };
            }
        };

        function mod5_shadow() {
            $(".baby-mod5-shadow").fadeIn(200)
        };

        function mod5_shadowout() {
            $(".baby-mod5-shadow").fadeOut(200)
        };

        function mod6_shadow() {
            $(".baby-cloud-overlay, .cloud-baby-shadow").fadeIn(500)
        };

        function mod6_shadowout() {
            $(".baby-cloud-overlay, .cloud-baby-shadow").fadeOut(200)
        };

        function formularight() {
            TweenLite.to($(".first-formulaTxt"), 0.5, {
                left: 1000
            })
        };

        function cloud_sheep() {
            TweenLite.to($(".cloud-sheep"), 1.5, {
                right: 120,
                top: 200
            })
        };

        function cloud_sheep_out() {
            TweenLite.to($(".cloud-sheep"), 0.5, {
                right: -550,
                top: 600
            })
        };

        function formulaleft() {
            TweenLite.to($(".first-formulaTxt"), 0.5, {
                left: 40
            })
        };

        //ANIMATION SEQUENCE

        /*
    		This will all be called once the DOM is ready. 
    	*/

        $(function() {

            /* scene 1 */

            firstFrameAnimations();
            continousfrombottom(".mom-standing", 0, 0, 30, 100, 0, 3, 4, 3, 6, 0, '', '');
            continousfrombottom(".babymod1-container", 0, 0, 0, 100, 0, 3, 4, 4, 3, 0, '', '');
            continousfrombottom(".module1-caption", 0, 0, -35, 800, 0, 3, 4, 6, 2, 200, '', '');
            continousfrombottom(".purple-bar", 0, 0, 570, 100, 0, 3, 4, 4, 3, 150, '', '');

            /* scene 2 */

            bottom(".mountain", 300, 20, 10, 2);
            continous_back(".baby-masked", 0, 490, 160, 300, 205, 3, 4.65, 3, 6, 350, '', '');
            continous_back(".curtain-container", 300, 400, -30, 390, 205, 5, 2, 6, 2, 200, '', '');
            continousfrombottom(".mom-mod2-container", 300, 490, 0, 300, 0, 3, 4, 3, 6, 0, '', '');
            continousfrombottom(".man2-container", 200, 510, -50, 500, 0, 3, 7.7, 0, 0, 0, formulaleft, formularight);
            continousfrombottom(".module2-footer", 580, 600, 1020, 492, 0, 3, 4, 4, 4, 900, '', '')
            

            /* scene 3 */

            left(".cloud-left", 0, 750, 10, 3, '', '');
            right(".cloud-right", 0, 750, 6, 3, '');
            continous_back(".baby-mod3", 550, 1176, 150, 500, 550, 2.95, 4, 3, 9, 550, '', '');
            continousfrombottom(".blanket", 540, 540, 390, 0, 0, 3, 4, 4, 4, 200, '', '');
            continousfrombottom(".fronthill-container", 1540, 1540, -50, 0, 0, 3, 4, 4, 4, 200, '', '');
            continousfrombottom(".module3-footer", 1000, 1176, 1730, 720, 1730, 9.8, 4, 4, 4, 1730, function() {
                TweenMax.to($(".bunny"), 0.5, {
                    bezier: _global.bezierCurves.scene3.bunny,
                    ease: Linear.easeNone
                });
            }, function() {
                TweenMax.to($(".bunny"), 0.5, {
                    bezier: _global.bezierCurves.scene3.bunnyOut,
                    ease: Linear.easeNone
                });
            });
            bottom(".sun", 0, -50, 5, 2, '');
            right(".bird-mod3", 1176, 1276, 2, 2, '');
            frombottom(".bird-mod3", 700, 0, 20, 16);
            //left(".cow", 0, 150, 5, 3, 0, -800);
            //left(".bunny", 1040, 980, 3, 4, 110, '');
            //fadein(".first-achooTxt", 150, 80);
            left(".first-achooTxt", 0, 1290, 3, 4, 100, '');
            frombottom2(".first-achooTxt", 7000, 260, 5, 4, 100);
            frombottom(".backhill-container", 850, 0, 9, 6);


            /* scene 5 */

            continous_back(".baby-mod5", 2210, 2520, 180, 450, 300, 2.35, 4, 3, 8, 180, mod5_shadow, mod5_shadowout);
            tobottom(".left-curtain-mod5", 7000, -30, 4, 3, -200);
            tobottom(".right-curtain-mod5", 7000, 0, 5, 3, -200);
            rotate(".light1", 'plus', 300, 360, 2, 350);
            rotate(".light2", 'plus', 300, 360, 2, 350);
            //continousfrombottom(".module5-floor", 2950, 2950, 300, 0, 0, 3, 4, 4, 5, 280, '', '');
            fadein(".module5-content-overlay", 100, -400);
            continousfrombottom(".module5-continous", 2950, 2950, 0, 0, 0, 3, 4, 4, 5, 280, '', '');
            //fadein(".first-bellyTxt", 300, '');
            //right(".first-bellyTxt", 0, 365, 4, 2, '');
            //bottom(".first-bellyTxt", 0, 310, 4, 2, '');
            continousfrombottom(".module5-footer", 2210, 2518, 3060, 600, 2947, 12.35, 4, 4, 4, 2947, function() {
                TweenLite.to($(".first-bellyTxt"), 0.5, {
                    right: 350,
                    top: 280,
                    opacity: 1
                });
            }, function() {
                TweenLite.to($(".first-bellyTxt"), 0.5, {
                    right: 120,
                    top: 136,
                    opacity: 0
                });
            });

            /* scene 6 */

            continous_back(".cloud-baby-container", 2900, 3246, 254, 500, 500, 2, 3.8, 3, 8, 350, mod6_shadow, mod6_shadowout);
            //bottom(".moon", 0, 0, 5, 4, -200);
            continousfrombottom(".frontCloud", 3000, 3246, 232, 800, 0, 2.75, 4, 4, 4, 80, '', '');
            continousfrombottom(".backCloud", 3150, 3246, 200, 800, 0, 2, 7, 4, 2, 150, cloud_sheep, cloud_sheep_out);
            continous_back(".star-container", 2500, 3560, 180, 150, 500, 6, 3, 6, 2, 300, '', '');
            //continousfrombottom(".cloud-sheep", 3380, 3380, 350, 200, 0, 6, 4, 4, 2, 150, '', '');
            //frombottom2(".container-text-mod6", 4000, 100, 2, 3, 350);

     
                continousfrombottom(".module6-footer", 3200, 3240, 3800, 685, 0, 4, 6, 4, 4, 3800, function() {
                    TweenLite.to($(".container-text-mod6"), 1, {
                        top: 100
                    })
                }, function() {
                    TweenLite.to($(".container-text-mod6"), 1, {
                        top: 600
                    })
                })
            

            /* scene 7 */

            continous_back(".bath-baby", 3700, 3971, 230, 300, 500, 3, 8.1, 4, 4, 0, '', '');
            continousfrombottom(".bathup-container", 3905, 3971, 370, 260, 0, 2, 11.7, 4, 4, 0, '', '');
            //right2(".banner-tag", 3600, 320, 1.35, 5);
            //continousfrombottom(".banner-tag", 4360, 4360, 0, 0, 0, 3, 4, 4, 3, 100, '', '');
            right2(".bird-container", 3630, 325, 1.0, 4);
            frombottom2(".bath-mom-container", 3540, 80, 5, 7, 220);
            //fadein(".megical-containers", 60, 200);
            //tobottom(".first-magicalTxt", 0, 0, 6, 3, 200);
            continousfrombottom(".module7-wall", 4360, 4360, 0, 0, 0, 3, 4, 6, 2, 100, '', '');
            continousfrombottom(".module7-continous", 4360, 4360, -20, 40, 0, 3, 4, 4, 4, 150, '', '');
            continousfrombottom(".module7-footer", 3900, 3965, 4520, 800, 0, 2, 4, 4, 4, 240, function() {
                TweenLite.to($(".first-magicalTxt"), 2, {
                    opacity: 1
                })
            }, function() {
                TweenLite.to($(".first-magicalTxt"), 2, {
                    opacity: 0
                })
            });

            /* scene 8 */

            continous(".the-baby-mod8", 4500, 4670, 150, 2000, 130, 3, 7.4);
            frombottom(".baby-crib-mod8", 6200, 20, 8, 2);
            frombottom3(".module8-bottom-img", 7000, 90, 3, 1.5, 230, priceFade._out, priceFade._in);
            frombottom3(".module8-can-container", 7000, 0, 2, 2, 300, function() {}, can.fade);

        });

    })(jQuery, TweenMax, TweenLite, TimelineLite, _nestle_global);


    function _nestle_global() {

        /*
    		This method will contain methods and properties that are to be accessed globally. This will be the only variable from this script available globally.
    	*/

        this.flutter = function(args) {
            var $target = $(args.target),
                zIndexDelay = 1000 + 1000 * args.delay;

            var timer = this.anim_toggleClass($target);

            setTimeout(function() {
                $target.css("z-index", 1000);
            }, zIndexDelay);

            TweenMax.to($target, args.duration, {
                bezier: args.path,
                ease: Linear.easeNone,
                delay: args.delay,
                onComplete: function() {
                    clearInterval(timer);
                }
            });
        }

        this.anim_toggleClass = function($target) {

            var frequency = ("undefined" !== typeof arguments[1]) ? arguments[1] : 100 + Math.floor(Math.random() * 50);

            return setInterval(function() {
                $target.toggleClass("show").toggleClass("hide");
            }, frequency);
        }

        this.anim_FadeElements = function($target) {
            var frequency = 1500;

            return setInterval(function() {
                $target.find(".open").fadeToggle();
                $target.find(".closed").fadeToggle();
            }, frequency);
        }

        /*
    		These are the curves used in animations throughout the page. The formatting of the curve objects is for greensock's animation platform: http://api.greensock.com/js/com/greensock/plugins/BezierPlugin.html
    	*/
        this.bezierCurves = {
            scene1: {
                greenButterfly: {
                    type: "cubic",
                    values: [{
                        x: 0,
                        y: 0
                    }, {
                        x: -600,
                        y: 350
                    }, {
                        x: 700,
                        y: 350
                    }, {
                        x: 200,
                        y: -400
                    }],
                    autoRotate: ["x", "y", "rotation", 0]
                },
                redButterfly: {
                    type: "cubic",
                    values: [{
                        x: 0,
                        y: 0
                    }, {
                        x: -550,
                        y: 350
                    }, {
                        x: 750,
                        y: 250
                    }, {
                        x: 250,
                        y: -400
                    }],
                    autoRotate: ["x", "y", "rotation", 0]
                }
            },
            scene3: {
                bunny: {
                    type: "quadratic",
                    values: [{
                        x: 0,
                        y: 0
                    }, {
                        x: 0,
                        y: -300
                    }, {
                        x: 300,
                        y: -300
                    }]
                },
                bunnyOut: {
                    type: "quadratic",
                    values: [{
                        x: 300,
                        y: -300
                    }, {
                        x: 0,
                        y: -300
                    }, {
                        x: 0,
                        y: 0
                    }]
                }
            },
            scene6: {
                sheep: {
                    type: "soft",
                    values: [{
                        x: 0,
                        y: -150
                    }, {
                        x: -100,
                        y: -75
                    }, {
                        x: -170,
                        y: 0
                    }, {
                        x: -170,
                        y: 0
                    }, {
                        x: -300,
                        y: 0
                    }, {
                        x: -500,
                        y: -150
                    }]
                }
            },
            scene7: {
                greenFish: {
                    type: "quadratic",
                    values: [{
                        x: 0,
                        y: 0
                    }, {
                        x: -105,
                        y: -200
                    }, {
                        x: -210,
                        y: 0
                    }],
                    autoRotate: ["x", "y", "rotation", -180]
                },
                blueFish: {
                    type: "quadratic",
                    values: [{
                        x: 0,
                        y: 0
                    }, {
                        x: 155,
                        y: -200
                    }, {
                        x: 200,
                        y: 0
                    }],
                    autoRotate: ["x", "y", "rotation", 30]
                }
            },
            scene8: {
                greenButterfly: {
                    type: "cubic",
                    values: [{
                        x: -100,
                        y: 0
                    }, {
                        x: 400,
                        y: 100
                    }, {
                        x: 900,
                        y: -500
                    }, {
                        x: 2000,
                        y: 0
                    }],
                    autoRotate: ["x", "y", "rotation", 0]
                },
                redButterfly: {
                    type: "cubic",
                    values: [{
                        x: -100,
                        y: 0
                    }, {
                        x: 450,
                        y: -500
                    }, {
                        x: 800,
                        y: 100
                    }, {
                        x: 2000,
                        y: 0
                    }],
                    autoRotate: ["x", "y", "rotation", 340]
                }
            }
        };

        return this;
    };
});